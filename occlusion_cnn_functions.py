import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.applications import inception_v3
from keras import regularizers
import matplotlib
from matplotlib import pyplot as plt
matplotlib.use("TkAgg")


# Pretrained model "InceptionV3 Model"
def pretrained_model(input_shape):
    pre_model = inception_v3.InceptionV3(include_top=False, weights='imagenet', input_shape=input_shape)
    final_model = Sequential()
    final_model.add(pre_model)
    # Freeze all but last 10 layers // Prevent Overfitting
    for layer in pre_model.layers[:-5]:
        layer.trainable = False
    final_model.add(Dropout(.5))
    final_model.add(Flatten())
    final_model.add(Dense(128))
    final_model.add(Dense(2, activation='softmax', kernel_regularizer=regularizers.l2(.01)))
    final_model.compile(loss='binary_crossentropy', optimizer=keras.optimizers.adam(lr=.0001), metrics=['accuracy'])

    return final_model


# Simple 3 Layer CNN Model
def simple_model(input_shape):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=input_shape, activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(.5))

    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(.5))

    model.add(Flatten())
    model.add(Dense(128, kernel_regularizer=regularizers.l2(.01)))
    model.add(Dense(2, activation='softmax'))
    model.compile(loss='binary_crossentropy', optimizer=keras.optimizers.adam(lr=.00001), metrics=['accuracy'])

    return model


# Plots metrics for model
def plot_model(model):
    # Shows accuracy measures over epochs
    plt.plot(model.history['acc'])
    plt.plot(model.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

    # Shows loss measures over epochs
    plt.plot(model.history['loss'])
    plt.plot(model.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()


# Saves model to path
def save_model(model_to_save, path):
    model_to_save.save_model('%s' % path)
    print('Saved model to: %s' % path)





