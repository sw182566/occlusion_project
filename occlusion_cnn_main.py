import cv2 as cv
import pandas as pd
import os
import numpy as np
import keras
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import resample
from sklearn.model_selection import train_test_split
from Working_occlusion_project.occlusion_cnn_functions import simple_model, save_model, plot_model
import matplotlib
matplotlib.use("TkAgg")


# # Read HDF Files // Needs to be done one time: See occlusion_cnn_image.py

direct = '/home/sam/Downloads/Aimages/'
number_of_files = len(os.listdir(direct))

lists = []
for i in range(0, number_of_files):
    lists.append(pd.read_hdf('/home/sam/PycharmProjects/Test/list_of_images%i' % i))
df = pd.concat(lists)

# # Remove all invalid images

bad = []
for i in range(0, len(df['image_path'])):
    image_array = cv.imread(df['image_path'][i])
    if image_array is None:
        bad.append(df.image_path[i])
        pass
    else:
        pass
df = df[~df.image_path.isin(bad)].reset_index(drop=True)
df_n_occluded = df[df.occluded == 1].reset_index(drop=True)
df_occluded = df[df.occluded == 0].reset_index(drop=True)
# FILTER IMAGES MANUALLY (Label bad images as 1)
# test2 = []
# for i in range(1000, len(df_n_occluded)):
#     print(df_n_occluded['image_path'][i])
#     test = cv.imread(df_n_occluded['image_path'][i])
#     if test is None:
#         test2.append('None%i' % i)
#     else:
#         if test.size == 0:
#             test2.append('None%i' %  i )
#         else:
#             test = cv.resize(test, (300, 300))
#             cv.imshow('t', test)
#             cv.waitKey(0)
#             cv.destroyAllWindows()
#             label = input('Label:')
#             if label == '1':
#                 print(i)
#                 test2.append(i)
#             else:
#                 pass
#     if i == 1100:
#         break
# print(test2)
good = [0, 6, 8, 22, 28, 30, 34, 35, 36, 46, 49, 55, 56, 57, 60, 61, 62, 63, 72, 74, 78, 79, 87, 88, 91, 92, 94,
        96, 100, 102, 105, 106, 107, 110, 113, 115, 120, 123, 124, 125, 127, 133, 138, 139, 154, 157, 165, 167, 169,
        170, 175, 177, 178, 180, 181, 185, 188, 190, 191, 192, 194, 195, 196, 197, 199, 200, 201, 204, 207, 209, 210,
        212, 213, 214, 215, 216, 217, 219, 221, 223, 225, 227, 230, 231, 232, 234, 235, 239, 242, 244, 245, 246, 248,
        250, 251, 252, 253, 256, 257, 258, 261, 265, 267, 270, 274, 281, 283, 286, 289, 291, 297, 301, 303, 307, 308,
        309, 310, 311, 316, 317, 320, 330, 334, 335, 336, 337, 338, 339, 341, 342, 343, 345, 346, 347, 348, 349, 350,
        352, 354, 355, 358, 359, 360, 361, 362, 363, 364, 365, 369, 370, 372, 379, 380, 381, 382, 383, 384, 385, 386,
        388, 389, 390, 392, 396, 397, 399, 400, 402, 403, 404, 406, 408, 411, 413, 414, 421, 422, 424, 425, 430, 437,
        447, 449, 450, 461, 465, 474, 477, 483, 484, 487, 492, 494, 495, 496, 497, 525, 539, 540, 542, 544, 545, 560,
        566, 567, 575, 602, 608, 609, 611, 612, 613, 614, 615, 616, 617, 618, 621, 622, 623, 625, 626, 627, 628, 629,
        630, 632, 635, 636, 638, 640, 641, 642, 643, 646, 648, 650, 651, 652, 654, 656, 657, 660, 661, 663, 665, 666,
        668, 669, 671, 672, 673, 674, 676, 677, 678, 679, 680, 682, 683, 684, 685, 687, 688, 689, 691, 697, 698, 700,
        701, 702, 705, 706, 707, 711, 712, 715, 716, 717, 718, 719, 720, 721, 724, 725, 731, 739, 740, 754, 755, 759,
        763, 764, 774, 776, 780, 782, 785, 786, 789, 795, 797, 801, 803, 807, 808, 809, 811, 813, 814, 815, 816, 817,
        818, 819, 823, 826, 827, 828, 829, 830, 831, 832, 833, 834, 836, 837, 838, 839, 841, 842, 843, 846, 847, 848,
        849, 850, 851, 852, 853, 854, 855, 856, 857, 859, 860, 864, 865, 868, 869, 870, 871, 875, 876, 882, 885, 891,
        892, 896, 897, 906, 910, 913, 916, 917, 918, 921, 923, 926, 927, 928, 930, 932, 935, 936, 937, 938, 939, 940,
        942, 944, 945, 946, 948, 954, 955, 957, 965, 966, 967, 974, 978, 988, 1001, 1003, 1012, 1021, 1023, 1028, 1030,
        1036, 1046, 1047, 1048, 1049, 1055, 1061, 1070, 1071, 1072, 1073, 1076, 1077, 1078, 1079, 1081, 1082, 1087,
        1088, 1089, 1093, 1094, 1095, 1101, 1107, 1108, 1109, 1111, 1112, 1113, 1114, 1115, 1116, 1119, 1120, 1121,
        1122, 1127, 1131, 1139, 1143, 1148, 1149, 1157, 1158, 1159, 1162, 1164, 1165, 1166, 1167, 1168, 1170, 1171,
        1173, 1175, 1176, 1177, 1178, 1179, 1182, 1184, 1185, 1186, 1188, 1189, 1191, 1194, 1200,1201, 1202, 1203, 1204,
        1205, 1207, 1210, 1211, 1212, 1214, 1215, 1216, 1217, 1218, 1220, 1222, 1225, 1226, 1240, 1246, 1256, 1257,
        1258, 1259, 1260, 1262, 1264, 1265, 1266, 1271, 1275, 1277, 1278, 1280, 1281, 1282, 1283, 1284, 1285, 1288,
        1290, 1304, 1320, 1321, 1329, 1342, 1347, 1355, 1361, 1363, 1366, 1369, 1371, 1373, 1377, 1382, 1389, 1390,
        1393, 1394, 1395, 1398, 1399, 1400, 1403, 1405, 1408, 1409, 1411, 1412, 1414, 1415, 1417, 1422, 1423, 1424,
        1425, 1426, 1430, 1432, 1433, 1439, 1442, 1443, 1446, 1449, 1450, 1451, 1457, 1461, 1464, 1466, 1467, 1471,
        1472, 1477, 1482, 1483, 1486, 1494, 1496, 1497, 1498, 1499, 1501, 1502, 1503, 1504, 1505, 1506, 1508, 1509,
        1512, 1513, 1514, 1519, 1520, 1521, 1522, 1523, 1524, 1525, 1526, 1527, 1529, 1530, 1531, 1532, 1533, 1535,
        1536, 1537, 1538, 1539, 1540, 1541, 1543, 1544, 1545, 1546, 1547, 1548, 1549, 1552, 1553, 1556, 1558, 1560,
        1564, 1565, 1566, 1567, 1571, 1575, 1579, 1580, 1588, 1596, 1600, 1601, 1606, 1607, 1613, 1614, 1616, 1618,
        1621, 1623, 1627, 1628, 1629, 1630, 1632, 1637, 1638, 1642, 1646, 1649, 1650, 1651, 1652, 1654, 1657]

df_n_occluded = df_n_occluded.iloc[good]
df_n_occluded = df_n_occluded.reset_index(drop='True')
df_majority_downsampled = resample(df_occluded, replace=False, n_samples=int(len(df_n_occluded)))
df_downsampled = pd.concat([df_majority_downsampled, df_n_occluded], ignore_index=True)

# # Resize images in down-sampled data frame

resized_arrays = []
for i in range(0, len(df_downsampled)):
    array = cv.imread(df_downsampled['image_path'][i])
    resized = cv.resize(array, (100, 100))
    # resized = cv.normalize(resized, 0, 255, norm_type=cv.NORM_MINMAX)
    resized = keras.applications.inception_v3.preprocess_input(resized)
    resized_arrays.append(resized)
x = np.array(resized_arrays)
y = keras.utils.to_categorical(df_downsampled['occluded'])
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.10)

# # GENERATE AUGMENTED DATA

batch_size = 32
data_gen = ImageDataGenerator(horizontal_flip=True, vertical_flip=True, rotation_range=20)
data_gen.fit(x_train)
val_gen = ImageDataGenerator()
val_gen = val_gen.flow(x_test, y_test, batch_size=batch_size)


# # Models and Fitting // All functions from occlusion_cnn_functions.py

simple_model = simple_model((100, 100, 3))
simple_hist = simple_model.fit_generator(data_gen.flow(x=x_train, y=y_train, batch_size=32),
                                         steps_per_epoch=len(x_train)//32, epochs=30, validation_data=val_gen,
                                         validation_steps=len(x_test)//20)
plot_model(simple_hist)
save_model(simple_model, 'model_name1')
#
# pretrained_model = pretrained_model((100, 100, 3))
# pretrained_hist = pretrained_model.fit_generator(data_gen.flow(x_train, y_train, batch_size=batch_size),
#                                                  steps_per_epoch=len(x_train) // batch_size, validation_data=val_gen,
#                                                  validation_steps=len(x_test) // 2, epochs=50)
# plot_model(pretrained_hist)
# save_model(pretrained_model, 'model_name2')





