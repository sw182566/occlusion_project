import pandas as pd
import os
import cv2 as cv
import matplotlib
matplotlib.use("TkAgg")
import numpy as np
from matplotlib import pyplot as plt
from Working_occlusion_project.occlusion_img import get_official_data, final_metrics, img_data,\
     machine_data
from sklearn import model_selection, preprocessing
from sklearn.metrics import confusion_matrix, f1_score
from sklearn.svm import SVC


# Get the proportion of edges using color segmentation #
def color_segmentation(cropped_img):
    percent_of_blue = []
    for r in range(0, len(cropped_img)):
        image = cropped_img[r]
        if image is None:
            percent_of_blue.append(None)
        else:
            hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
            lower = (80, 10, 0)
            upper = (150, 255, 255)
            mask = cv.inRange(hsv, lower, upper)
            result = cv.bitwise_and(hsv, hsv, mask=mask)
            count = cv.countNonZero(mask)
            size = image.size
            percent_of_blue.append(count/size)
    return percent_of_blue


# Gets the proportion of edges of a cropped image #
def calc_edge_hist_metrics(cropped_imgs):
    edges = []
    for i in range(0, len(cropped_imgs)):
        if cropped_imgs[i] is None:
            edges.append(None)
        else:
            gray = cv.Canny(cropped_imgs[i], 180, 250)
            # cv.imshow('gray', gray)
            # cv.waitKey(0)
            total = gray.shape[0] * gray.shape[1]
            ratio = 100 * cv.countNonZero(gray)/total
            edges.append(ratio)
    return edges


def calc_avg_hist_metrics(cropped_imgs):
    hist = []
    for i in range(len(cropped_imgs)):
        if cropped_imgs[i] is None:
            hist.append([None, None, None])
            pass
        else:
            avg_color_per_row = np.average(cropped_imgs[i], axis=0)
            avg_color = np.average(avg_color_per_row, axis=0)
            hist.append(avg_color)
    return hist


def test_pattern(cropped_img):
    distance = []
    for r in range(0, len(cropped_img)):
        image = cropped_img[r]
        if image is None:
            distance.append(None)
        else:
            # For clear images
            hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
            lower = (90, 10, 0)
            upper = (150, 255, 200)
            mask = cv.inRange(hsv, lower, upper)
            result = cv.bitwise_and(hsv, hsv, mask=mask)
            h, s, v = cv.split(result)
            count = cv.countNonZero(mask)
            size = image.size
            image = cv.Canny(v, 200, 275)
            # image = cv.Canny(image, 180, 250)
            circles = cv.HoughCircles(image, cv.HOUGH_GRADIENT, 1, 1, 200, 100, 1, 10, 30)
            # array = circles[0].ravel()
            # center = (array[0], array[1])
            # radius = array[2]
            # cv.circle(cropped_img[r], center, radius, (255,255,255), 1)
            # cv.imshow('test', result)
            # cv.imshow('test2', cv.resize(cropped_img[r], (100,100)))
            # cv.waitKey(0)
            predicted_center = []
            if len(circles) == 1:
                array = circles[0].ravel()
                center = (array[0], array[1])
                predicted_center.append(center)
                true_center = (int(image.shape[1] / 2), int(image.shape[0] / 2))
                dist2 = np.linalg.norm(np.array(true_center) - np.array(predicted_center[0]))
                distance.append(dist2)
            else:
                distance.append(10)
    return distance


def machine_learning_svc(x, y):
    x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=.20)
    # x_train, x_val, y_train, y_val = model_selection.train_test_split(x_train, y_train, test_size=0.30, random_state=1)
    s_x_train = preprocessing.scale(x_train)
    # s_x_val = preprocessing.scale(x_val)
    s_x_test = preprocessing.scale(x_test)
    model = SVC(kernel='rbf', gamma='auto', random_state=j)
    model.fit(s_x_train, y_train)
    y_pred = model.predict(s_x_test)
    score = model.score(s_x_test, y_test)
    f1 = f1_score(y_test, y_pred, average='binary')
    confusion = confusion_matrix(y_test, y_pred)
    incorrect = np.where(np.equal(y_pred, y_test))
    return score


# Set rescale (Depends on screen) #
rs_factor = 2
# Set Directory to work with #
direct = '/home/sam/Downloads/Aimages/'
number_of_files = len(os.listdir(direct))
# Compile all image data and do predictions based on metric (Histogram + Logistic) #
compiled_df = []

# DO NOT TOUCH UNLESS CHANGING THE DATA #
# for k in range(0, number_of_files):
#     # Returns cropped image array and respective labels #
#     img, label = function(direct, rs_factor, k)
#     # Creates data frame 'list_of_images' #
#     read_pd(label, k)
# Metrics: Depends on the metrics we are going to use #
# Returns data frame 'list_of_images' #
df = get_official_data()
# # Returns list of image paths for work #
images = img_data(df)
edge = calc_edge_hist_metrics(images)
hist_avg = calc_avg_hist_metrics(images)
color_se = color_segmentation(images)
dist = test_pattern(images)
final_metric = final_metrics(df, hist_avg, edge, color_se, dist)
compiled_df.append(final_metric)
test = pd.concat(compiled_df, ignore_index=True)
test = test.dropna()
test.occluded = test.occluded.astype(int)
if __name__ == '__main__':
    svc_scores = []
    for j in range(0, 1):
        variables, labels = machine_data(test)
        svc_scores.append(machine_learning_svc(variables, labels))
    plt.subplot(1, 2, 2), plt.hist(svc_scores)
    plt.show()
    percent_right_svc = np.average(svc_scores)
    print("Score:%f" % percent_right_svc)
