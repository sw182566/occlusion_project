import pandas as pd
import os


# Read in files from directory in list/data frame (Given Labelled Contours in h5 file)
def load_files(direct):
    file_list = []
    for entry in os.listdir(direct):
        file_list.append(str(entry))
    file_df = pd.DataFrame(file_list)
    file_df.columns = ['File_name']
    file_df.to_hdf('list_of_files', key='df')





