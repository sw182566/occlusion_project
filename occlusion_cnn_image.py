import pandas as pd
import cv2 as cv
import numpy as np
import os
from Working_occlusion_project.occlusion_file import load_files
from aeroimg.annotated import AnnotatedImg


def function(directory, rs_factor, index):
    # Load the directory files
    load_files(directory)
    df = pd.read_hdf('list_of_files', 'df')
    # Loop through each image in file
    img_file = df['File_name'][index]
    aimg_path = '/home/sam/Downloads/Aimages/%s' % img_file
    # Read in image (given by Scott)
    a_img = AnnotatedImg.from_hdf5_file(aimg_path)
    # Image data from AnnotatedImage
    geo_image = a_img.georef_img
    geo_image = geo_image.get_layer('visible')
    img_arr_xy = geo_image.arr
    img_arr_yx = img_arr_xy.swapaxes(0, 1)
    size = img_arr_yx.shape[0]*img_arr_yx.shape[1]
    # Resize imported image
    test_img = cv.resize(img_arr_yx, (int(np.size(img_arr_yx,1)/ rs_factor), int(np.size(img_arr_yx,0)/rs_factor)))
    # No contour img
    clear_test_img = test_img.copy()
    # Contour data from AnnotatedImage
    geom_collection = a_img.geom_collections
    geom_collection = geom_collection['contours']
    # Drawing bounding bbox rectangles given the contours
    d = 0
    occluded = []
    clear_crop_img = []
    list_of_bboxs = [contour.bbox for contour in geom_collection]
    # Set buffer scale
    b_factor = 1.005
    # Create buffered image
    os.mkdir('/home/sam/PycharmProjects/Test/buffer_images/buffer_images%i' % index)
    for bbox in list_of_bboxs:
        # Create buffer image bounding box
        e_ymax = int((bbox.ymax / rs_factor) * b_factor)
        e_ymin = int(bbox.ymin / rs_factor * (1 / b_factor))
        e_xmax = int((bbox.xmax / rs_factor) * b_factor)
        e_xmin = int(bbox.xmin / rs_factor * (1 / b_factor))
        clear_crop_img.append(clear_test_img[e_ymin:e_ymax, e_xmin:e_xmax].copy())
        # Write to buffer_images
        file_name = "/home/sam/PycharmProjects/Test/buffer_images/buffer_images%i/file_%d.jpg" % (index, d)
        cv.imwrite(file_name, clear_crop_img[d])
        test = a_img.geom_collections['contours']
        # Test is LabelledGeoref
        test = test.geom_list
        test = test[d]
        test = test._get_labels()
        occluded.append(test.values[0])
        d = d + 1
    return clear_crop_img, occluded


# Read clear_crop_img paths into pandas data frame #
def read_pd(y, index):
    paths = []
    for i in range(len(os.listdir('/home/sam/PycharmProjects/Test/buffer_images/buffer_images%i' % index))):
        path = "/home/sam/PycharmProjects/Test/buffer_images/buffer_images%i/file_%d.jpg" % (index, i)
        paths.append(path)
    dictionary = dict(zip(paths, y))
    # img_list = pd.DataFrame.from_dict(dictionary, orient='index')
    img_list = pd.DataFrame(dictionary.items())
    img_list = img_list.rename(index=str, columns={0: 'image_path', 1: 'occluded'})
    # print(img_list)
    img_list.to_hdf('/home/sam/PycharmProjects/Test/list/list_of_images%i' % index, key='df')

# # I manually created /list and /buffer_images file into my working directory.
# # Run this only once
# direct = '/home/sam/Downloads/Aimages/'
# number_of_files = len(os.listdir(direct))
# for i in range(0, number_of_files):
#     img, label = function(direct, 2, i)
#     read_pd(label, i)
