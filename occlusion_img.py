import cv2 as cv
import numpy as np
import pandas as pd
import matplotlib
import os
from Working_occlusion_project.occlusion_file import load_files
from aeroimg.annotated import AnnotatedImg
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
from sklearn.utils import resample


# Loads and Returns in bbbox and cropped images with labels
def function(directory, rs_factor, index):

    # Load the directory files
    load_files(directory)
    df = pd.read_hdf('list_of_files', 'df')
    # Loop through each image in file
    img_file = df['File_name'][index]
    aimg_path = '/home/sam/Downloads/Aimages/%s' % img_file

    # Read in image (given by Scott)
    a_img = AnnotatedImg.from_hdf5_file(aimg_path)

    # Image data from AnnotatedImage
    geo_image = a_img.georef_img
    geo_image = geo_image.get_layer('visible')
    img_arr_xy = geo_image.arr
    img_arr_yx = img_arr_xy.swapaxes(0, 1)

    size = img_arr_yx.shape[0]*img_arr_yx.shape[1]

    # Resize imported image
    test_img = cv.resize(img_arr_yx, (int(np.size(img_arr_yx,1)/ rs_factor), int(np.size(img_arr_yx,0)/rs_factor)))

    # No contour img
    clear_test_img = test_img.copy()

    # Contour data from AnnotatedImage
    geom_collection = a_img.geom_collections
    geom_collection = geom_collection['contours']

    # Drawing bounding bbox rectangles given the contours
    d = 0
    crop_img = []
    occluded = []
    c_occluded = []
    clear_crop_img = []
    list_of_bboxs = [contour.bbox for contour in geom_collection]

    # Set buffer scale
    b_factor = 1.005

    for contour in geom_collection:
        result = contour.geom
        result = result.points
        result = tuple((int(x[0]/rs_factor), int(x[1]/rs_factor))for x in result)
        x,y,w,z= cv.boundingRect(np.int32([result]))
        # cv.rectangle(clear_test_img, (x,y), (x+w, y+z), (0, 0, 0), 1)
        # cv.polylines(clear_test_img, np.int32([result]), 1,  (0, 255, 0))
    # Create buffered image
    os.mkdir('/home/sam/PycharmProjects/Test/buffer_images%i' % index)
    for bbox in list_of_bboxs:
        # Create buffer image bounding box
        e_ymax = int(bbox.ymax / rs_factor * (b_factor))
        e_ymin = int(bbox.ymin / rs_factor * (1 / b_factor))
        e_xmax = int(bbox.xmax / rs_factor * (b_factor))
        e_xmin = int(bbox.xmin / rs_factor * (1 / b_factor))
        # cv.rectangle(test_img, (e_xmin, e_ymin), (e_xmax, e_ymax), (0, 0, 255), 1)
        # crop_img.append(test_img[e_ymin:e_ymax, e_xmin:e_xmax].copy())
        clear_crop_img.append(clear_test_img[e_ymin:e_ymax, e_xmin:e_xmax].copy())
        # Write to buffer_images
        file_name = "/home/sam/PycharmProjects/Test/buffer_images%i/file_%d.jpg" % (index, d)
        cv.imwrite(file_name, clear_crop_img[d])
        test = a_img.geom_collections['contours']
        # Test is LabelledGeoref
        test = test.geom_list
        test = test[d]
        test = test._get_labels()
        occluded.append(test.values[0])
        # if clear_crop_img[d].size == 0:
        #     print(d)
        #     c_occluded.append('')
        # else:
        #     print(d)
        #     cv.imshow('Image', clear_crop_img[d])
        #     cv.waitKey(0)
        #     cv.destroyAllWindows()
        #     label = input("Label:")
        #     c_occluded.append(label)
        # cv.putText(test_img, str(occluded[d]), (int(bbox.xmin/rs_factor), int(bbox.ymin/rs_factor)), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1)
        d = d + 1
    return clear_crop_img, occluded


# Read clear_crop_img paths into pandas data frame #
def read_pd(y, index):
    paths = []
    for i in range(len(os.listdir('/home/sam/PycharmProjects/Test/buffer_images%i' % index))):
        path = "/home/sam/PycharmProjects/Test/buffer_images%i/file_%d.jpg" % (index, i)
        paths.append(path)
    dictionary = dict(zip(paths, y))
    # img_list = pd.DataFrame.from_dict(dictionary, orient='index')
    img_list = pd.DataFrame(dictionary.items())
    img_list = img_list.rename(index=str, columns={0: 'image_path', 1: 'occluded'})
    # print(img_list)
    img_list.to_hdf('list_of_images%i' % index, key='df')


# # Color histogram for each of the cropped images #
def plot_color(cropped_img, labels):
    for r in range(0, len(cropped_img)):
        plt.subplot(len(cropped_img), 1, r+1)
        hist = cv.calcHist([cropped_img[r]], [0], None, [256], [0, 256])
        plt.plot(hist)
        plt.ylabel("%d" % labels[r])
    plt.show()


# # Edge detection for each of the cropped images #
def edge_detection(cropped_img, labels):
    for r in range(0,len(cropped_img)):
        edge = cv.Canny(cropped_img[r], 150, 200)
        plt.subplot(1, 2, 1), plt.imshow(edge, cmap='gray')
        plt.subplot(1, 2, 2), plt.imshow(cropped_img[r], cmap='gray')
        plt.text(2, 2, "%d" % labels[r], fontsize=20)
        plt.show()


# # Reads in the hdf pandas file of images #
def get_official_data():
    lists = []
    for i in range(0, 27):
        lists.append(pd.read_hdf('/home/sam/PycharmProjects/Test/list_of_images%i' % i))
    df = pd.concat(lists, ignore_index=True)
    return df


# # Creates the data frame for the machine learning functions #
#   Uses: metric- color histogram, edge - number of edge pixels #
def final_metrics(df, metric, metric2, metric3, metric4):
    df['metric1'] = metric
    temp = pd.DataFrame(df['metric1'].values.tolist(), columns=['one','two','three'])
    final_df = pd.concat([df.reset_index(drop=True), temp.reset_index(drop=True)], axis=1, ignore_index=False)
    final_df = final_df.drop('metric1', 1)
    final_df['metric2'] = metric2
    final_df['metric3'] = metric3
    final_df['metric4'] = metric4
    return final_df


# # Image Paths
def img_data(dfs):
    cropped_img_class = []
    for i in range(0, len(dfs['image_path'])):
        cropped_img_class.append(cv.imread(dfs['image_path'][i]))
    return cropped_img_class


# # Downsample/Upsampled Data and choose which metrics you want
def machine_data(dfs):
    df = dfs
    df_occluded = df[df.occluded == 0]
    df_n_occluded = df[df.occluded == 1].reset_index(drop=True)
    df_majority_downsampled = resample(df_occluded, replace=False, n_samples=int(len(df_n_occluded)))
    df_downsampled = pd.concat([df_majority_downsampled, df_n_occluded], ignore_index=True)
    x = df_downsampled.drop(['occluded', 'image_path', 'metric2'], axis=1)
    y = df_downsampled['occluded']
    return x, y

