import os
from keras.models import load_model
import numpy as np
import keras
import pandas as pd
import cv2 as cv
from aeroimg.annotated import AnnotatedImg

# # ENTER IMAGE FILE AND PREDICTION MODEL HERE
aimg_path = '/home/sam/Downloads/Aimages_Test/1-28-0028.h5'
model = load_model('augmented_clean_data_model')


# Read in image (given by Scott)
a_img = AnnotatedImg.from_hdf5_file(aimg_path)
# Image data from AnnotatedImage
geo_image = a_img.georef_img
geo_image = geo_image.get_layer('visible')
img_arr_xy = geo_image.arr
img_arr_yx = img_arr_xy.swapaxes(0, 1)
geom_collection = a_img.geom_collections
geom_collection = geom_collection['contours']
list_of_bboxs = [contour.bbox for contour in geom_collection]
d = 0
clear_crop_img = []
rs_factor = 2
b_factor = 1
c_occluded = []
for bbox in list_of_bboxs:
    test = a_img.geom_collections['contours']
    e_ymax = int(bbox.ymax / rs_factor * b_factor)
    e_ymin = int(bbox.ymin / rs_factor * 1 / b_factor)
    e_xmax = int(bbox.xmax / rs_factor * b_factor)
    e_xmin = int(bbox.xmin / rs_factor * 1 / b_factor)
    test_img = cv.resize(img_arr_yx, (int(np.size(img_arr_yx, 1) / rs_factor), int(np.size(img_arr_yx, 0) / rs_factor)))
    clear_test_img = test_img.copy()
    clear_crop_img.append(clear_test_img[e_ymin:e_ymax, e_xmin:e_xmax].copy())
    test = test.geom_list
    test = test[d]
    labels = test._get_labels()
    if clear_crop_img[d].size == 0:
        pass
    else:
        resize = cv.resize(clear_crop_img[d], (100, 100))
        resize = np.array(resize).reshape(1, 100, 100, 3)
        resized = cv.normalize(resize, 0, 255, norm_type=cv.NORM_MINMAX)
        resized = resize // 255
        resized = keras.applications.inception_v3.preprocess_input(resize)
        prediction = model.predict(resized)
        if prediction[0][0] < prediction[0][1]:
            print('Predict: Not occluded', prediction[0][1])
        else:
            print('Predict: Occluded', prediction[0][0])
        print(prediction)
        cv.imshow('Image', clear_crop_img[d])
        cv.waitKey(0)
        cv.destroyAllWindows()
    d = d+1





